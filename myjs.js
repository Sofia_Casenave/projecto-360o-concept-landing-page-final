//scrollToTop

$(document).ready(function () {
  $(".scrollTriggerOne").click(function (event) {
    event.preventDefault();
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $("#targetOne").offset().top,
        },
        1
      );
  });
  $(".scrollTriggerTwo").click(function (event) {
    event.preventDefault();
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $("#targetTwo").offset().top,
        },
        1
      );
  });
  $(".scrollTriggerThree").click(function (event) {
    event.preventDefault();
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $("#targetThree").offset().top,
        },
        1
      );
  });

  //scrollToTop and autoscroll with setTimeout

  $(".scrollTriggerFour").click(function (event) {
    event.preventDefault();
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $("#targetFour").offset().top,
        },
        1
      );
    setTimeout("jQuery('#targetFour').hide();", 4000);
  });

  //scrollToTop

  $(".scrollTriggerFive").click(function (event) {
    event.preventDefault();
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $("#targetSix").offset().top,
        },
        1
      );
  });

  //Animations

  $("#triangle").click(function () {
    if ($(this).hasClass("animation-tri")) {
      $("#triangle, #square, #circle").removeClass("animation-tri");
      $("#text, #submit").removeClass("hide");
      $("#square, #circle").removeClass("event");
      $("#trianglePer").removeClass("show");
      $("#idLogo").removeClass("idLeft");
      $("#descriptionTriangle").removeClass("show");
      $("#logoImage").removeClass("hide").addClass("show");
      $("#triCinema").addClass("hide").removeClass("show");
      $("#triangleShow").addClass("show").removeClass("hide");
      $("#triangleHide").addClass("hide").removeClass("show");
    } else {
      $("#triangle, #square, #circle").addClass("animation-tri");
      $("#text, #submit").addClass("hide");
      $("#square, #circle").addClass("event");
      $("#trianglePer").addClass("show");
      $("#idLogo").addClass("idLeft");
      $("#logoImage").addClass("hide").removeClass("show");
      $("#descriptionTriangle").addClass("show");
      $("#triCinema").addClass("show").removeClass("hide");
      $("#triangleShow").addClass("hide").removeClass("show");
      $("#triangleHide").addClass("show").removeClass("hide");
    }
  });

  $("#square").click(function () {
    if ($(this).hasClass("animation-square")) {
      $("#square, #triangle, #circle").removeClass("animation-square");
      $("#text, #submit").removeClass("hide");
      $("#triangle, #circle").removeClass("event");
      $("#squarePer").removeClass("show");
      $("#idLogo").removeClass("idLeft");
      $("#descriptionSquare").removeClass("show");
      $("#squareExp").addClass("hide").removeClass("show");
      $("#squareShow").addClass("show").removeClass("hide");
      $("#squareHide").addClass("hide").removeClass("show");
    } else {
      $("#square, #triangle, #circle").addClass("animation-square");
      $("#text, #submit").addClass("hide");
      $("#triangle, #circle").addClass("event");
      $("#squarePer").addClass("show");
      $("#idLogo").addClass("idLeft");
      $("#descriptionSquare").addClass("show");
      $("#squareExp").addClass("show").removeClass("hide");
      $("#squareShow").addClass("hide").removeClass("show");
      $("#squareHide").addClass("show").removeClass("hide");
    }
  });

  $("#circle").click(function () {
    if ($(this).hasClass("animation-circle")) {
      $("#square, #triangle, #circle").removeClass("animation-circle");
      $("#text, #submit").removeClass("hide");
      $("#triangle, #square").removeClass("event");
      $("#circlePer").removeClass("show");
      $("#idLogo").removeClass("idLeft");
      $("#descriptionCircle").removeClass("show");
      $("#circleMus").addClass("hide").removeClass("show");
      $("#circleShow").addClass("show").removeClass("hide");
      $("#circleHide").addClass("hide").removeClass("show");
    } else {
      $("#square, #triangle, #circle").addClass("animation-circle");
      $("#text, #submit").addClass("hide");
      $("#triangle, #square").addClass("event");
      $("#circlePer").addClass("show");
      $("#idLogo").addClass("idLeft");
      $("#descriptionCircle").addClass("show");
      $("#circleMus").addClass("show").removeClass("hide");
      $("#circleShow").addClass("hide").removeClass("show");
      $("#circleHide").addClass("show").removeClass("hide");
    }
  });
});
